package com.amazingtalk.test.base

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlin.collections.ArrayList

/**
 * Created by genius on 2018/3/19.
 */
//GridView基礎類別
abstract class BaseGridViewAdapter(context: Context) : BaseAdapter() {

    var mAry = ArrayList<Any>()
    val inflater: LayoutInflater = context.getSystemService(
        Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    protected abstract fun getMyView(position: Int,
                                     convertView: View?,
                                     parent: ViewGroup?): View

    fun setDataArray(ary: Any?) {

        ary?.let {

            mAry.clear()
            mAry.addAll(it as ArrayList<*>)
            notifyDataSetChanged()
        }
    }

    override fun getCount(): Int {
        return mAry.size
    }

    override fun getItem(position: Int): Any {
        return mAry[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int,
                         convertView: View?,
                         parent: ViewGroup): View {
        return getMyView(position, convertView, parent)
    }

}