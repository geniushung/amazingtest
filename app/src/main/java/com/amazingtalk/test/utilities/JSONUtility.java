package com.amazingtalk.test.utilities;

import android.text.TextUtils;

import com.amazingtalk.test.BuildConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

/**
 * JSON相關Utility
 *
 * @author Jimmy
 */
@SuppressWarnings({"UnusedReturnValue", "unused"})
public class JSONUtility {

    /**
     * Get JSONObject from String
     *
     * @param data String
     * @return JSONObject
     * @author Jimmy
     */
    public static JSONObject getJSONObject(String data) {
        JSONObject json = null;
        try {
            json = new JSONObject(data);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

        return json;
    }

    /**
     * Get JSONObject from JSONArray
     *
     * @param array JSONArray
     * @return JSONObject
     * @author Jimmy
     */
    public static JSONObject getJSONObject(JSONArray array, int position) {
        JSONObject json = null;
        try {
            json = array.getJSONObject(position);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

        return json;
    }

    /**
     * Get JSONObject from JSONObject
     *
     * @param json JSONObject
     * @return JSONObject
     * @author Jimmy
     */
    public static JSONObject getJSONObject(JSONObject json, String key) {
        JSONObject jsonChild = null;
        try {
            jsonChild = json.getJSONObject(key);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

        return jsonChild;
    }

    /**
     * Get JSONArray from String
     *
     * @param data String
     * @return JSONArray
     * @author Jimmy
     */
    public static JSONArray getJSONArray(String data) {
        JSONArray json = null;
        try {
            json = new JSONArray(data);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

        return json;
    }

    /**
     * 在JSON增加Object
     *
     * @param json JSONObject
     * @param key key
     * @param value value
     * @return JSONObject
     * @author Jimmy
     */
    public static JSONObject putStringToJSONObject(JSONObject json,
                                                   String key,
                                                   String value) {
        try {
            json.put(key, value);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
        return json;
    }

    public static JSONObject putObjectToJSONObject(JSONObject json,
                                                   String key,
                                                   JSONObject value) {
        try {
            json.put(key, value);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
        return json;
    }

    public static JSONArray putJSONObjectToJSONArray(JSONArray jary,
                                                     JSONObject value) {
        try {
            jary.put(value);
        } catch (NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

        return jary;
    }

    public static JSONObject putArrayToJSONObject(JSONObject json,
                                                  String key,
                                                  JSONArray value) {
        try {
            json.put(key, value);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
        return json;
    }

    public static JSONObject putIntToJSONObject(JSONObject json,
                                                String key, int value) {
        try {
            json.put(key, value);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
        return json;
    }

    public static JSONObject putFloatToJSONObject(JSONObject json,
                                                  String key,
                                                  BigDecimal value) {
        try {
            json.put(key, value.doubleValue());
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
        return json;
    }

    /**
     * 在JSON增加Object
     *
     * @param json JSONObject
     * @param key key
     * @param value value
     * @return JSONObject
     * @author Jimmy
     */
    public static JSONObject putBooleanToJSONObject(JSONObject json,
                                                    String key,
                                                    boolean value) {
        try {
            json.put(key, value);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
        return json;
    }

    /**
     * Get the String value from key in JSONObject
     *
     * @param json JSONObject
     * @param key search key
     * @return value. If return is null, it means that there has no key in json
     * object
     * @author Jimmy
     */
    public static String getStringFromJSONObject(JSONObject json,
                                                 String key) {
        String value = "";

        if (json == null) {
            return value;
        }

        try {
            value = json.getString(key);
            if (TextUtils.isEmpty(value) || value.equals("null")) {
                value = "";
            }
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

        return value;
    }

    /**
     * Get the String value from key in JSONObject
     *
     * @param json JSONObject
     * @param position index
     * @return value. If return is null, it means that there has no key in json
     * object
     * @author Jimmy
     */
    public static String getStringFromJSONArray(JSONArray json,
                                                int position) {

        String value = "";

        if (json == null) {
            return value;
        }

        try {
            value = json.getString(position);
            if (TextUtils.isEmpty(value) || value.equals("null")) {
                value = "";
            }
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

        return value;
    }

    /**
     * Get the boolean value from key in JSONObject
     *
     * @param json JSONObject
     * @param key search key
     * @return value. If return is null, it means that there has no key in json
     * object
     * @author Jimmy
     */
    @SuppressWarnings("SameParameterValue")
    public static boolean getBooleanFromJSONObject(JSONObject json,
                                                   String key) {
        boolean value = false;

        if (json == null) {
            return false;
        }

        try {
            value = json.getBoolean(key);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

        return value;
    }

    /**
     * Get the int value from key in JSONObject
     *
     * @param json JSONObject
     * @param key search key
     * @return value. If return is null, it means that there has no key in json
     * object
     * @author Jimmy
     */
    @SuppressWarnings("SameParameterValue")
    public static int getIntFromJSONObject(JSONObject json,
                                           String key) {
        int value = -1;

        if (json == null) {
            return value;
        }

        try {
            value = json.getInt(key);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

        return value;
    }

    /**
     * Get the Long value from key in JSONObject
     *
     * @param json JSONObject
     * @param key search key
     * @return value. If return is null, it means that there has no key in json
     * object
     * @author Jimmy
     */
    public static Long getLongFromJSONObject(JSONObject json, String key) {
        long value = 0L;

        if (json == null) {
            return value;
        }

        try {
            value = json.getLong(key);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

        return value;
    }

    /**
     * Get the Double value from key in JSONObject
     *
     * @param json JSONObject
     * @param key search key
     * @return value. If return is null, it means that there has no key in json
     * object
     * @author Jimmy
     */
    public static Double getDoubleFromJSONObject(JSONObject json,
                                                 String key) {
        Double value = null;

        if (json == null) {
            return null;
        }

        try {
            value = json.getDouble(key);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

        return value;
    }

    /**
     * Get the JSONArray from key in JSONObject
     *
     * @param json JSONObject
     * @param key search key
     * @return JSONArray. If return is null, it means that there has no key in
     * json object
     * @author Jimmy
     */
    public static JSONArray getJSONArrayFromJSONObject(JSONObject json,
                                                       String key) {
        JSONArray array = null;

        if (json == null) {
            return null;
        }

        try {
            array = json.getJSONArray(key);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

        return array;
    }

    /**
     * Get the JSONObkect from String data
     *
     * @param data String data
     * @return JSONObject
     * @author Jimmy
     */
    public static JSONObject getJSONObjectFromString(String data) {
        JSONObject json = null;
        try {
            json = new JSONObject(data);
        } catch (JSONException | NullPointerException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

        return json;
    }

}
