package com.amazingtalk.test.utilities

import android.util.Log
import com.amazingtalk.test.BuildConfig

class Utility {

    companion object{

        private const val TAG = "jimmy"

        fun log(msg: String) {
            if (BuildConfig.DEBUG) Log.i(TAG, msg)
        }

    }
}