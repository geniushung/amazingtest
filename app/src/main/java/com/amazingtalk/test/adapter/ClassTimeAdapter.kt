package com.amazingtalk.test.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.amazingtalk.test.R
import com.amazingtalk.test.base.BaseGridViewAdapter
import com.amazingtalk.test.data.TimeData
import com.amazingtalk.test.databinding.CellTimeBinding

//時間adapter
class ClassTimeAdapter(context: Context) : BaseGridViewAdapter(context) {

    private val mContext = context
    private lateinit var binding : CellTimeBinding

    override fun getMyView(position: Int,
                           convertView: View?,
                           parent: ViewGroup?): View {

        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.cell_time,
                parent,
                false)

        val timeData = mAry[position] as ArrayList<*>

        val ary : ArrayList<TimeData> = ArrayList()

        for(i in 0 until timeData.size) {
            ary.add(timeData[i] as TimeData)
        }

        val adapter = TimeAdapter(mContext)

        binding.rcvTime.layoutManager = LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false)

        binding.rcvTime.adapter = adapter

        adapter.setDataArray(ary)

        return binding.root
    }
}