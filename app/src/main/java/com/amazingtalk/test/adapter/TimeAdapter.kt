package com.amazingtalk.test.adapter

import android.content.Context
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.amazingtalk.test.R
import com.amazingtalk.test.base.BaseRecyclerViewAdapter
import com.amazingtalk.test.data.TimeData
import com.amazingtalk.test.databinding.CellTimeListBinding

//時間列表adapter
class TimeAdapter(context : Context) :
        BaseRecyclerViewAdapter<TimeAdapter.MyTimeViewHolder>(context) {

    private val mContext = context
    private lateinit var mBinding : CellTimeListBinding

    override fun onCreateMyViewHolder(
            parent: ViewGroup?,
            viewType: Int): RecyclerView.ViewHolder {

        val resId = R.layout.cell_time_list

        mBinding = DataBindingUtil.inflate(mInflater, resId, parent, false)

        return MyTimeViewHolder(mBinding)
    }

    override fun onBindMyViewHolder(
            holder: RecyclerView.ViewHolder?,
            position: Int) {

        val data : TimeData = getDataArray()[position] as TimeData
        val myTimeViewHolder : MyTimeViewHolder = holder as MyTimeViewHolder
        myTimeViewHolder.setTimeData(mContext, data)
    }

    class MyTimeViewHolder(binding: CellTimeListBinding) :
            RecyclerView.ViewHolder(binding.root) {

        private val mBinding = binding

        fun setTimeData(context: Context, data : TimeData) {

            val start = data.start.split("T")[1].replace(":00Z", "")

            mBinding.txtTime.text = start

            val type = data.type

            val black = ActivityCompat.getColor(context, R.color.colorBlack)

            mBinding.txtTime.setTextColor(black)

            val gray = ActivityCompat.getColor(context, R.color.colorGray)
            val green = ActivityCompat.getColor(context, R.color.teal_200)

            if(type == TimeData.Type.AVAILABLE)
                mBinding.txtTime.setTextColor(gray)
            else if(type == TimeData.Type.BOOKED)
                mBinding.txtTime.setTextColor(green)
        }
    }
}