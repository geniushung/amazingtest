package com.amazingtalk.test.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.amazingtalk.test.R
import com.amazingtalk.test.base.BaseGridViewAdapter
import com.amazingtalk.test.data.DayData
import com.amazingtalk.test.databinding.CellDayBinding

//日期adapter
class ClassDayAdapter(context: Context) : BaseGridViewAdapter(context)  {

    private val mContext = context
    private lateinit var binding : CellDayBinding

    override fun getMyView(position: Int,
                           convertView: View?,
                           parent: ViewGroup?): View {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.cell_day,
            parent,
            false)

        val dayDate = mAry[position] as DayData

        binding.txtDay.text = dayDate.day
        binding.txtWeek.text = getWeekStr(dayDate.week)

        val workColor = ContextCompat.getColor(mContext, R.color.teal_200)
        val blackColor = ContextCompat.getColor(mContext, R.color.colorBlack)

        if(dayDate.enable) {
            binding.viewWork.setBackgroundColor(workColor)
            binding.txtWeek.setTextColor(blackColor)
            binding.txtDay.setTextColor(blackColor)
        }

        return binding.root
    }

    private fun getWeekStr(str : String) : String {
        return when(str) {
            "0"->mContext.getString(R.string.day_sun)
            "1"->mContext.getString(R.string.day_mon)
            "2"->mContext.getString(R.string.day_tue)
            "3"->mContext.getString(R.string.day_wed)
            "4"->mContext.getString(R.string.day_thu)
            "5"->mContext.getString(R.string.day_fri)
            "6"->mContext.getString(R.string.day_sat)
            else -> ""
        }
    }
}