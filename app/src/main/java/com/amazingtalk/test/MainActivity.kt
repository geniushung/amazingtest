package com.amazingtalk.test

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.amazingtalk.test.adapter.ClassDayAdapter
import com.amazingtalk.test.adapter.ClassTimeAdapter
import com.amazingtalk.test.api.ApiEvent
import com.amazingtalk.test.api.FakeAPI
import com.amazingtalk.test.data.DayData
import com.amazingtalk.test.data.TimeData
import com.amazingtalk.test.databinding.ActivityMainBinding
import com.amazingtalk.test.utilities.JSONUtility
import com.amazingtalk.test.utilities.Utility
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

//主頁面
class MainActivity : AppCompatActivity(), View.OnClickListener, ApiEvent {

    private var dayArray : ArrayList<Any> = ArrayList()
    private lateinit var dayAdapter : ClassDayAdapter
    private lateinit var timeAdapter : ClassTimeAdapter
    private lateinit var binding : ActivityMainBinding
    private lateinit var myCal : Calendar
    private lateinit var api : FakeAPI
    private var count = 7

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.mainActivity = this

        initView()

        initApi()

        getTimeZone()

        setInitDayArray()
    }

    private fun initView() {
        binding.imgbtPrev.isEnabled = false
        dayAdapter = ClassDayAdapter(this)
        binding.gvDay.adapter = dayAdapter
        timeAdapter = ClassTimeAdapter(this)
        binding.gvTime.adapter = timeAdapter
    }

    private fun initApi() {
        api = FakeAPI(this)
        api.setApiEvent(this)
    }

    private fun getTimeZone() {

        val timeBy = getString(R.string.main_time_by)
        val timeShow = getString(R.string.main_time_show)

        val tz = TimeZone.getDefault()

        val displayStr = tz.displayName
        val zone = tz.getDisplayName(false, TimeZone.SHORT)

        val str = "$timeBy $displayStr (${zone}) $timeShow"

        binding.txtTimeZone.text = str
    }

    private fun setInitDayArray() {

        val cal : Calendar = Calendar.getInstance()

        dayArray.clear()

        val week = cal[Calendar.DAY_OF_WEEK] - 1

        count = 7

        for(i in 0 until 7) {

            val subCal : Calendar = Calendar.getInstance()

            subCal.add(Calendar.DAY_OF_MONTH, (i - week))
            val subDay = subCal[Calendar.DAY_OF_MONTH]
            val subWeek = subCal[Calendar.DAY_OF_WEEK] - 1

            if(i < week) {
                val data = DayData(subWeek.toString(), subDay.toString())
                data.enable = false
                dayArray.add(data)
                count -= 1
                continue
            }

            val data = DayData(subWeek.toString(), subDay.toString())

            dayArray.add(data)

            if(i == 6) myCal = subCal
        }

        dayAdapter.setDataArray(dayArray)

        setTimeInterval(cal, myCal)

        api.startGetFakeData(cal, count)
    }

    private fun setTimeInterval(startCal : Calendar, endCal : Calendar) {

        val def = Locale.getDefault()
        val formateStr = "%d-%02d-%02d"

        val startYear = startCal[Calendar.YEAR]
        val startMonth = startCal[Calendar.MONTH] + 1
        val startDay = startCal[Calendar.DAY_OF_MONTH]

        val stDay = String.format(def,
                formateStr,
                startYear,
                startMonth,
                startDay)

        val endYear = endCal[Calendar.YEAR]
        val endMonth = endCal[Calendar.MONTH] + 1
        val endDay = endCal[Calendar.DAY_OF_MONTH]

        val edDay = String.format(def,
                formateStr,
                endYear,
                endMonth,
                endDay)

        val str = "$stDay - $edDay"

        binding.txtInterval.text = str
    }

    override fun onClick(v: View?) {

        when(v?.id ?: 0) {
            R.id.imgbtPrev -> {
                previousWeek()
            }
            R.id.imgbtNext -> {
                nextWeek()
            }
        }
    }

    private fun nextWeek() {

        binding.imgbtPrev.isEnabled = true

        count = 7

        val ccal : Calendar = Calendar.getInstance()

        ccal.set(Calendar.YEAR, myCal[Calendar.YEAR])
        ccal.set(Calendar.MONTH, myCal[Calendar.MONTH])
        ccal.set(Calendar.DAY_OF_MONTH, myCal[Calendar.DAY_OF_MONTH])
        ccal.add(Calendar.DAY_OF_MONTH, 1)

        dayArray.clear()

        for(i in 0 until 7) {

            myCal.add(Calendar.DAY_OF_MONTH, 1)

            val subDay = myCal[Calendar.DAY_OF_MONTH]
            val subWeek = myCal[Calendar.DAY_OF_WEEK] - 1

            val data = DayData(subWeek.toString(), subDay.toString())

            dayArray.add(data)
        }

        dayAdapter.setDataArray(dayArray)

        setTimeInterval(ccal, myCal)

        api.startGetFakeData(ccal, 7)
    }

    private fun previousWeek() {

        val cal : Calendar = Calendar.getInstance()

        val mi = cal.timeInMillis

        dayArray.clear()

        myCal.add(Calendar.DAY_OF_MONTH, -14)

        val ccal : Calendar = Calendar.getInstance()

        ccal.set(Calendar.YEAR, myCal[Calendar.YEAR])
        ccal.set(Calendar.MONTH, myCal[Calendar.MONTH])
        ccal.set(Calendar.DAY_OF_MONTH, myCal[Calendar.DAY_OF_MONTH])
        ccal.add(Calendar.DAY_OF_MONTH, 1)

        for(i in 0 until 7) {

            myCal.add(Calendar.DAY_OF_MONTH, 1)

            val subDay = myCal[Calendar.DAY_OF_MONTH]
            val subWeek = myCal[Calendar.DAY_OF_WEEK] - 1

            val data = DayData(subWeek.toString(), subDay.toString())

            if(myCal.timeInMillis < mi) {
                setInitDayArray()
                dayAdapter.setDataArray(dayArray)
                binding.imgbtPrev.isEnabled = false
                return
            }

            dayArray.add(data)
        }

        count = 7

        dayAdapter.setDataArray(dayArray)

        setTimeInterval(ccal, myCal)
    }

    override fun getFakeData(json: JSONObject) {

        Utility.log(json.toString())

        val ajary = JSONUtility.getJSONArrayFromJSONObject(json, "available")
        val bjary = JSONUtility.getJSONArrayFromJSONObject(json, "booked")

        refactorData(ajary, bjary)

    }

    private fun refactorData(ajary: JSONArray, bjary: JSONArray) {

        val allDataAry : MutableList<MutableList<TimeData>> = mutableListOf()

        val dataAry1 : ArrayList<TimeData> = ArrayList()
        val dataAry2 : ArrayList<TimeData> = ArrayList()
        val dataAry3 : ArrayList<TimeData> = ArrayList()
        val dataAry4 : ArrayList<TimeData> = ArrayList()
        val dataAry5 : ArrayList<TimeData> = ArrayList()
        val dataAry6 : ArrayList<TimeData> = ArrayList()
        val dataAry7 : ArrayList<TimeData> = ArrayList()

        val ary = arrayOf(dataAry1,
                dataAry2,
                dataAry3,
                dataAry4,
                dataAry5,
                dataAry6,
                dataAry7)

        var st = 7 - count

        var cmp = ""

        for(i in 0 until ajary.length()) {

            val obj = JSONUtility.getJSONObject(ajary, i)

            var dd = JSONUtility.getStringFromJSONObject(obj, "start")

            if(i == 0) cmp = dd.split("T")[0]

            val data = TimeData()
            data.start = JSONUtility.getStringFromJSONObject(obj, "start")
            data.end = JSONUtility.getStringFromJSONObject(obj, "end")
            data.type = TimeData.Type.AVAILABLE

            if(data.start.contains(cmp)) {
                ary[st].add(data)
            }
            else {
                dd = JSONUtility.getStringFromJSONObject(obj, "start")
                cmp = dd.split("T")[0]
                st += 1
                ary[st].add(data)
            }
        }

        st = 7 - count

        for(i in 0 until bjary.length()) {

            val obj = JSONUtility.getJSONObject(bjary, i)

            var dd = JSONUtility.getStringFromJSONObject(obj, "start")

            if(i == 0) cmp = dd.split("T")[0]

            val data = TimeData()
            data.start = JSONUtility.getStringFromJSONObject(obj, "start")
            data.end = JSONUtility.getStringFromJSONObject(obj, "end")
            data.type = TimeData.Type.BOOKED

            if(data.start.contains(cmp)) {
                ary[st].add(data)
            }
            else {
                dd = JSONUtility.getStringFromJSONObject(obj, "start")
                cmp = dd.split("T")[0]
                st += 1
                ary[st].add(data)
            }
        }

        for(i in ary.indices) {

            val sortedList = ary[i].sortedWith(compareBy(TimeData::start))

            val subAryList : ArrayList<TimeData> = ArrayList()

            for(j in sortedList.indices) subAryList.add(sortedList[j])

            allDataAry.add(subAryList)
        }

        timeAdapter.setDataArray(allDataAry)

    }
}