package com.amazingtalk.test.api

import org.json.JSONObject

//api callBack
interface ApiEvent {
    fun getFakeData(json : JSONObject)
}