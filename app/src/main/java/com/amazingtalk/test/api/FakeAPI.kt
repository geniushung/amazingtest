@file:Suppress("UNUSED_PARAMETER")

package com.amazingtalk.test.api

import android.content.Context
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.random.Random

//假的API
class FakeAPI(context: Context){

    private var event : ApiEvent? = null

    fun setApiEvent(ev : ApiEvent?) { event = ev }

    fun startGetFakeData(startCal : Calendar, count : Int) {

        val json = JSONObject()

        val aAry = JSONArray()
        val bAry = JSONArray()

        for(i in 0 until count) {

            startCal.add(Calendar.DAY_OF_MONTH, i)

            val day = getDateStr(startCal)

            for(j in 0 until 4) {

                val subJson = JSONObject()

                val dayAdd = Random.nextInt(0, 2)

                if(dayAdd == 0) {

                    when(j) {
                        0->{
                            subJson.put("start", day + "00:30:00Z")
                            subJson.put("end", day + "02:00:00Z")
                            aAry.put(subJson)
                        }
                        1->{
                            subJson.put("start", day + "10:30:00Z")
                            subJson.put("end", day + "11:00:00Z")
                            aAry.put(subJson)
                        }
                        2->{
                            subJson.put("start", day + "13:00:00Z")
                            subJson.put("end", day + "14:00:00Z")
                            aAry.put(subJson)
                        }
                        3->{
                            subJson.put("start", day + "05:30:00Z")
                            subJson.put("end", day + "07:00:00Z")
                            aAry.put(subJson)
                        }
                        else->{}
                    }

                }

            }

            for(j in 0 until 4) {

                val subJson = JSONObject()

                val dayAdd = Random.nextInt(0, 2)

                if(dayAdd == 0) {

                    when(j) {
                        0->{
                            subJson.put("start", day + "11:00:00Z")
                            subJson.put("end", day + "13:00:00Z")
                            bAry.put(subJson)
                        }
                        1->{
                            subJson.put("start", day + "14:00:00Z")
                            subJson.put("end", day + "15:00:00Z")
                            bAry.put(subJson)
                        }
                        2->{
                            subJson.put("start", day + "07:00:00Z")
                            subJson.put("end", day + "08:00:00Z")
                            bAry.put(subJson)
                        }
                        3->{
                            subJson.put("start", day + "11:30:00Z")
                            subJson.put("end", day + "13:00:00Z")
                            bAry.put(subJson)
                        }
                        else->{}
                    }
                }
            }
        }

        json.put("available", aAry)
        json.put("booked", bAry)

        event?.getFakeData(json)

    }

    private fun getDateStr(cal : Calendar) : String {

        val y = cal[Calendar.YEAR]
        val m = cal[Calendar.MONTH] + 1
        val d = cal[Calendar.DAY_OF_MONTH]

        return String.format(Locale.getDefault(), "%d-%02d-%02dT", y, m, d)
    }

}