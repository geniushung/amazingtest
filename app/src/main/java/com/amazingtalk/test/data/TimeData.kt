package com.amazingtalk.test.data

//時間Data class
class TimeData : Comparable<TimeData> {

    enum class Type {
        NONE,
        AVAILABLE,
        BOOKED
    }

    var start = ""
    var end = ""
    var type : Type = Type.NONE

    override fun compareTo(other: TimeData): Int {

        var str = other.start.split("T")[1].replace("Z", "")
        str = str.replace(":", "")

        val compareage : Int = str.toInt()

        var nowStr = this.start.split("T")[1].replace("Z", "")
        nowStr = nowStr.replace(":", "")

        if (nowStr.toInt() > compareage) return 1
        if (nowStr.toInt() < compareage) return -1
        return 0
    }
}